#include "Chords/Maj7.hpp"

namespace Diatonic {
namespace Chords {

	std::ostream& Maj7::PrintName(std::ostream& os) const
	{
		return os << "Maj7";
	}

}}
