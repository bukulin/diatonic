#ifndef SINGLETONHOLDER_HPP
#define SINGLETONHOLDER_HPP

#include <stdlib.h>

namespace Diatonic {
namespace Pattern {

	template <typename T>
	class SingletonHolder
	{
	private:
		SingletonHolder();

	public:
		static T& GetInstance()
		{
			if (theInstance)
				return *theInstance;
			theInstance = new T;
			atexit(DeleteInstance);
			return *theInstance;
		}

	private:
		static void DeleteInstance()
		{
			if (theInstance == nullptr)
				return;
			delete theInstance;
			theInstance = nullptr;
		}

	private:
		static T* theInstance;
	};

	template <typename T>
	T* SingletonHolder<T>::theInstance = nullptr;

}}

#endif
