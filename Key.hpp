#ifndef KEY_HPP
#define KEY_HPP

#include <boost/optional.hpp>
#include <string>

namespace Diatonic {

	enum class Key {
		C = 0,
		Csharp = 1, Dflat = 1,
		D = 2,
		Dsharp = 3, Eflat = 3,
		E = 4,
		F = 5,
		Fsharp = 6, Gflat = 6,
		G = 7,
		Gsharp = 8, Aflat = 8,
		A = 9,
		Asharp = 10, Bflat = 10,
		B = 11,
		__NUMBER_OF_NOTES = 12
	};

	enum class Modifier {
		Sharp,
		Flat
	};

	boost::optional<Key> StringToKey(std::string&& key);
	std::string KeyToString(const Key key,
				const Modifier modifier);

	Key operator + (const Key root, const int semitones);
	Modifier DescribeModifier(const Key note);
}

#endif
