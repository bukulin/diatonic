#ifndef MIN7B5_HPP
#define MIN7B5_HPP

#include "Chords/Chord.hpp"

namespace Diatonic {
namespace Chords {

	class Min7b5 : public Chord
	{
	public:
		using Chord::Chord;

	public:
		std::ostream& PrintName(std::ostream& os) const override;
	};

}}

#endif
