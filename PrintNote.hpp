#ifndef PRINTNOTE_HPP
#define PRINTNOTE_HPP

#include "Key.hpp"

#include <ostream>
#include <vector>
#include <iterator>

namespace Diatonic {
namespace IO {

	template <typename Container>
	std::ostream& Print(std::ostream& os, const Container& notes)
	{
		const auto modifier = DescribeModifier(notes.at(0));

		std::vector<std::string> names(notes.size());
		std::transform(notes.cbegin(), notes.cend(),
			       names.begin(),
			       [modifier](const Key key){
				       return KeyToString(key, modifier);
			       });

		std::copy(names.cbegin(), names.cend(),
			  std::ostream_iterator<std::string>(os, " "));

		return os;
	}

}}

#endif
