#include "Key.hpp"
#include "Degree.hpp"

#include "Scales/MajorScale.hpp"

#include "Chords/Chord.hpp"

#include <iostream>

using namespace std;
using namespace Diatonic;

static void PrintHeader(std::ostream& os)
{
	os << "Notation of chord types" << endl
	     << "\tmaj7, 7, min7, min7b5" << endl;

	os << "Notation of notes" << endl
	     << "\tC, D, E, F, G, A, B" << endl
	     << "\tDb, Eb, Gb, Ab, Bb" << endl
	     << "\tC#, D#, F#, G#, A#" << endl;
}

static Key GetKey(std::ostream& os, std::istream& is)
{
	boost::optional<Key> key;

	while(!key)
	{
		os << "Please enter a key or press Ctrl-c to exit: ";
		string keyName;
		is >> keyName;
		key = StringToKey(move(keyName));
	}

	return *key;
}

static Degree GetDegree(std::ostream& os, std::istream& is)
{
	boost::optional<Degree> degree;

	while(!degree)
	{
		os << "Chord at scale degree: ";
		unsigned int degreeOrder;
		is >> degreeOrder;
		is.clear();
		is.ignore();
		degree = ToDegree(degreeOrder);
	}

	return *degree;
}

int main(void)
{
	PrintHeader(std::cout);
	auto key = GetKey(std::cout, std::cin);
	Scales::MajorScale scale(key);
	cout << scale << endl;

	auto degree = GetDegree(std::cout, std::cin);
	const auto chord = scale.MakeChord(degree);
	cout << *chord << endl;

	return 0;
}
