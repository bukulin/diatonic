#include "Chord.hpp"

#include "PrintNote.hpp"

using namespace std;

namespace Diatonic {
namespace Chords {

	Chord::Chord(Chord::Tones&& tones)
		: itsTones(move(tones))
	{}

	Chord::~Chord() = default;

	ostream& Chord::Print(ostream& os) const
	{
		PrintName(os) << ": ";
		return IO::Print(os, itsTones);
	}

	ostream& operator << (ostream& os, const Chord& chord)
	{
		return chord.Print(os);
	}

}}
