#ifndef MIN7_HPP
#define MIN7_HPP

#include "Chords/Chord.hpp"

namespace Diatonic {
namespace Chords {

	class Min7 : public Chord
	{
	public:
		using Chord::Chord;

	public:
		std::ostream& PrintName(std::ostream& os) const override;
	};

}}


#endif
