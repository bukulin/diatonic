#include "Degree.hpp"

#include "CircularShift.hpp"
#include "Tools.hpp"

namespace Diatonic {

	using Math::CircShift;

	Degree operator + (const Degree root, const int offset)
	{
		return CircShift<Degree,
				 Degree::__NUMBER_OF_DEGREES>(root,
							      offset);
	}

	boost::optional<Degree> ToDegree(const int degreeOrder)
	{
		const auto transformed = degreeOrder - 1;

		if (transformed < ToUType(Degree::I) ||
		    transformed > ToUType(Degree::VII))
			return boost::none;

		Degree degree = static_cast<Degree>(transformed);
		return boost::optional<Degree>(degree);
	}
}
