#ifndef CIRCULARSHIFT_HPP
#define CIRCULARSHIFT_HPP

#include "Tools.hpp"

namespace Diatonic {
namespace Math {

	template <typename T, T MAX>
	T CircShift (const T root, const int offset)
	{
		const auto rootValue = ToUType(root);
		auto newValue = rootValue + offset;
		if (newValue >= ToUType(MAX))
			newValue -= ToUType(MAX);
		if (newValue < 0)
			newValue += ToUType(MAX);
		return static_cast<T>(newValue);
	}


}}

#endif
