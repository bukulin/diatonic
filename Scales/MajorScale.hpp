#ifndef MAJORSCALE_HPP
#define MAJORSCALE_HPP

#include "Scale.hpp"

#include <array>

namespace Diatonic {
namespace Scales {

	class MajorScale final : public Scale
	{
	private:
		using Scale = std::array<Key, 7>;
	public:
		explicit MajorScale(const Key root);
	private:
		static Scale BuildScale(const Key root);

	public:
		std::ostream& Print(std::ostream& os) const override;
		Key GetNoteAt(const Degree degree) const override;
		size_t GetNumberOfDegrees() const override;
		ManagedChord MakeChord(const Degree degree) const override;

	private:
		const Scale itsNotes;
	};

}}

#endif
