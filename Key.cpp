#include "Key.hpp"

#include "SingletonHolder.hpp"
#include "CircularShift.hpp"

#include <map>
#include <vector>

namespace Diatonic {

	using namespace std;

	class KeyNameMap
	{
	private:
		using KeyNamePair = pair<string, Key>;
		using PairVector  = vector<KeyNamePair>;
	public:
		KeyNameMap()
			: itsSharpMap{
			{"C", Key::C},
			{"C#", Key::Csharp},
			{"D", Key::D},
			{"D#", Key::Dsharp},
			{"E", Key::E},
			{"F", Key::F},
			{"F#", Key::Fsharp},
			{"G", Key::G},
			{"G#", Key::Gsharp},
			{"A", Key::A},
			{"A#", Key::Asharp},
			{"B", Key::B}}

			, itsFlatMap{
			{"C", Key::C},
			{"Db", Key::Dflat},
			{"D", Key::D},
			{"Eb", Key::Eflat},
			{"E", Key::E},
			{"F", Key::F},
			{"Gb", Key::Gflat},
			{"G", Key::G},
			{"Ab", Key::Aflat},
			{"A", Key::A},
			{"Bb", Key::Bflat},
			{"B", Key::B}}
		{}

	public:
		boost::optional<Key> ToKey(string&& name) const
		{
			auto sharpKey = FindIn(itsSharpMap, move(name));
			if (sharpKey)
				return sharpKey;

			auto flatKey = FindIn(itsFlatMap, move(name));
			if (flatKey)
				return flatKey;

			return boost::none;
		}

	private:
		boost::optional<Key> FindIn(const PairVector& haystack,
					    string&& name) const
		{
			const auto it =
				find_if(haystack.cbegin(), haystack.cend(),
					[&name](const KeyNamePair& pair){
						return pair.first == name;
					});
			if (it == haystack.cend())
				return boost::none;
			return boost::optional<Key>(it->second);
		}


	public:
		std::string ToString(const Key key,
				     const Modifier modifier) const
		{
			if (modifier == Modifier::Sharp)
				return FindIn(itsSharpMap, key);
			else
				return FindIn(itsFlatMap, key);
			}

	private:
		std::string FindIn(const PairVector& haystack,
				   const Key key) const
		{
			const auto it =
				find_if(haystack.cbegin(), haystack.cend(),
					[&key](const KeyNamePair& pair){
						return pair.second == key;
					});
			return it->first;
		}

	private:
		const PairVector itsSharpMap;
		const PairVector itsFlatMap;
	};

	using KeyNameMapSingleton = Pattern::SingletonHolder<KeyNameMap>;

	boost::optional<Key> StringToKey(string&& name)
	{
		return KeyNameMapSingleton::GetInstance().ToKey(move(name));
	}

	std::string KeyToString(const Key key,
				const Modifier modifier)
	{
		return KeyNameMapSingleton::GetInstance().
			ToString(key, modifier);
	}

	Key operator + (const Key root, const int semitones)
	{
		return Math::CircShift<Key,
				       Key::__NUMBER_OF_NOTES>(root,
							       semitones);
	}

	Modifier DescribeModifier(const Key note)
	{
		static const map<Key, Modifier> modifierMap {
			{Key::C, Modifier::Sharp},

			{Key::G, Modifier::Sharp},
			{Key::D, Modifier::Sharp},
			{Key::A, Modifier::Sharp},
			{Key::E, Modifier::Sharp},
			{Key::B, Modifier::Sharp},

			{Key::F, Modifier::Flat},
			{Key::Bflat, Modifier::Flat},
			{Key::Eflat, Modifier::Flat},
			{Key::Aflat, Modifier::Flat},
			{Key::Dflat, Modifier::Flat},
			{Key::Gflat, Modifier::Flat},
		};

		return modifierMap.find(note)->second;
	}
}
