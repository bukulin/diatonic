#ifndef CHORD_HPP
#define CHORD_HPP

#include <memory>
#include <ostream>

#include <cstddef>

namespace Diatonic {

	enum class Degree;
	enum class Key;

namespace Chords {

	class Chord
	{
	protected:
		using Tones = std::array<Key, 4>;

	public:
		explicit Chord(Tones&& tones);
		virtual ~Chord();

	public:
		std::ostream& Print(std::ostream& os) const;

	private:
		virtual std::ostream& PrintName(std::ostream& os) const = 0;

	private:
		Tones itsTones;
	};


	std::ostream& operator << (std::ostream& os, const Chord& chord);

}}

#endif
