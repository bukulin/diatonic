#ifndef DEGREE_HPP
#define DEGREE_HPP

#include <boost/optional.hpp>

namespace Diatonic {

	enum class Degree
	{
		I = 0,
		II = 1,
		III = 2,
		IV = 3,
		V = 4,
		VI = 5,
		VII = 6,
		__NUMBER_OF_DEGREES = 7
	};

	Degree operator + (const Degree root, const int offset);
	boost::optional<Degree> ToDegree(const int degreeOrder);
}

#endif
