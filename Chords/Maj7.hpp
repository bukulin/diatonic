#ifndef MAJ7_HPP
#define MAJ7_HPP

#include "Chords/Chord.hpp"

namespace Diatonic {
namespace Chords {

	class Maj7 : public Chord
	{
	public:
		using Chord::Chord;

	public:
		std::ostream& PrintName(std::ostream& os) const override;
	};

}}

#endif
