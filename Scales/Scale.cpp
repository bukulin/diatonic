#include "Scales/Scale.hpp"

namespace Diatonic {
namespace Scales {

	using namespace std;

	Scale::~Scale() = default;

	ostream& operator << (ostream& os, const Scale& scale)
	{
		return scale.Print(os);
	}

}}
