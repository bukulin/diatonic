#ifndef TOOLS_HPP
#define TOOLS_HPP

#include <memory>
#include <type_traits>

namespace Diatonic {

	template <typename Enum>
	constexpr typename std::underlying_type<Enum>::type ToUType(const Enum e)
	{
		return static_cast<
			typename std::underlying_type<Enum>::type>(e);
	}

	template<typename T, typename... Args>
	std::unique_ptr<T> make_unique(Args... args)
	{
		return std::unique_ptr<T>(
			new T(std::forward<Args>(args)...));
	}

}

#endif
