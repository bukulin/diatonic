#ifndef DOM7_HPP
#define DOM7_HPP

#include "Chords/Chord.hpp"

namespace Diatonic {
namespace Chords {

	class Dom7 : public Chord
	{
	public:
		using Chord::Chord;

	public:
		std::ostream& PrintName(std::ostream& os) const override;
	};

}}


#endif
