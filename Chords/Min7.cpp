#include "Chords/Min7.hpp"

namespace Diatonic {
namespace Chords {

	std::ostream& Min7::PrintName(std::ostream& os) const
	{
		return os << "Min7";
	}

}}
