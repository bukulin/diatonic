#include "Chords/Dom7.hpp"

namespace Diatonic {
namespace Chords {

	std::ostream& Dom7::PrintName(std::ostream& os) const
	{
		return os << "Dom7";
	}

}}
