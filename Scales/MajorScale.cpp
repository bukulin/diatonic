#include "Scales/MajorScale.hpp"

#include "Chords/Dom7.hpp"
#include "Chords/Maj7.hpp"
#include "Chords/Min7.hpp"
#include "Chords/Min7b5.hpp"

#include "Degree.hpp"
#include "Key.hpp"
#include "PrintNote.hpp"
#include "Tools.hpp"

#include <array>
#include <algorithm>
#include <iterator>

namespace Diatonic {
namespace Scales {

	using namespace std;

	MajorScale::MajorScale(const Key root)
		: itsNotes(BuildScale(root))
	{}

	MajorScale::Scale MajorScale::BuildScale(const Key root)
	{
		Scale scale;
		scale[0] = root;
		scale[1] = scale[0] + 2;
		scale[2] = scale[1] + 2;
		scale[3] = scale[2] + 1;
		scale[4] = scale[3] + 2;
		scale[5] = scale[4] + 2;
		scale[6] = scale[5] + 2;

		return scale;
	}

	ostream& MajorScale::Print(ostream& os) const
	{
		return IO::Print(os, itsNotes);
	}

	Key MajorScale::GetNoteAt(const Degree degree) const
	{
		return itsNotes[ToUType(degree)];
	}

	size_t MajorScale::GetNumberOfDegrees() const
	{
		return itsNotes.size();
	}

	Scale::ManagedChord MajorScale::MakeChord(const Degree degree) const
	{
		const auto offsets = {0, 2, 4, 6};
		array<Degree, 4> degrees;
		transform(begin(offsets), end(offsets),
			  degrees.begin(),
			  [&degree](const int offset) {
				  return degree + offset;
			  });

		array<Key, 4> tones;
		transform(begin(degrees), end(degrees),
			  tones.begin(),
			  [this](const Degree degree) {
				  return GetNoteAt(degree);
			  });

		switch(degree)	// TODO this could be calculated
		{
		default:
		case Degree::I:
			return make_unique<Chords::Maj7>(move(tones));
		case Degree::II:
			return make_unique<Chords::Min7>(move(tones));
		case Degree::III:
			return make_unique<Chords::Min7>(move(tones));
		case Degree::IV:
			return make_unique<Chords::Maj7>(move(tones));
		case Degree::V:
			return make_unique<Chords::Dom7>(move(tones));
		case Degree::VI:
			return make_unique<Chords::Min7>(move(tones));
		case Degree::VII:
			return make_unique<Chords::Min7b5>(move(tones));
		}
	}
}}
