#ifndef SCALE_HPP
#define SCALE_HPP

#include <ostream>
#include <memory>

namespace Diatonic {

	enum class Degree;
	enum class Key;

namespace Chords {

	class Chord;

}

namespace Scales {

	class Scale
	{
	protected:
		using ManagedChord = std::unique_ptr<Chords::Chord>;

	protected:
		Scale() = default;
	public:
		Scale(const Scale&) = delete;
		virtual ~Scale();

	public:
		virtual std::ostream& Print(std::ostream& os) const = 0;
		virtual Key GetNoteAt(const Degree degree) const = 0;
		virtual size_t GetNumberOfDegrees() const = 0;
		virtual ManagedChord MakeChord(const Degree degree) const = 0;
	};

	std::ostream& operator << (std::ostream& os, const Scale& scale);

}}

#endif
