#include "Chords/Min7b5.hpp"

namespace Diatonic {
namespace Chords {

	std::ostream& Min7b5::PrintName(std::ostream& os) const
	{
		return os << "Min7b5";
	}

}}
